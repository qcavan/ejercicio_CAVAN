#include <stdio.h>
#include <stdlib.h>


typedef enum espectral_t espectral_t;
enum espectral_t
{
    O, B, A, F, G, K, M
};

void espectral(){
    espectral_t estrella;
    int res =0;
    int temp = 0;
    printf("Temperatura del cuerpo celeste: ");
    res = scanf("%d", &temp);

    if(1700<=temp && temp<3200){
        estrella = M;
        printf("El cuerpo celeste es rojo");
    }else if(3200<=temp && temp<4600){
        estrella = K;
        printf("El cuerpo celeste es naranja");
    }else if(4600<=temp && temp<5700){
        estrella = G;
        printf("El cuerpo celeste es amarillo");
    }else if(5700<=temp && temp<7100){
        estrella = F;
        printf("El cuerpo celeste es Blanco amarillento");
    }else if(7100<=temp && temp<9600){
        estrella = A;
        printf("El cuerpo celeste es blanco");
    }else if(9600<=temp && temp<28000){
        estrella = B;
        printf("El cuerpo celeste es Blanco azulado");
    }else if(28000<=temp && temp<50000){
        estrella = O;
        printf("El cuerpo celeste es azul");
    }else{
        printf("La temperatura no es correcta");
    }


};


int main()
{
    espectral();
    return 0;
}
