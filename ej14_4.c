#include <stdio.h>
#include <stdlib.h>

int busqueda_binaria(const int v[], /* vector donde buscar */
                      size_t n,      /* largo del vector (dispensable) */
                      int objetivo,  /* elemento a buscar */
                      size_t izq,    /* franja del vector donde buscar, */
                      size_t der)   /* delimitada por izq y der */
{

   int centro,inf=izq,sup=der;
   while(inf<=sup){
      centro=((sup-inf)/2)+inf;
      if(v[centro]==objetivo)       return centro;
      else if(objetivo < v[centro]) sup=centro-1;
      else                           inf=centro+1;
   }
   return -1;
}


int main()
{
    int i=0;
    int v[] = {1,2,3,4,5,6};
    i = busqueda_binaria(v, 6, 3,0,5);
    printf("%d", i);
    return 0;
}
