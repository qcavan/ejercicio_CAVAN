#include <stdio.h>
#include <stdlib.h>

void segtohour(int segundos){
    int hours=0, min=0;
    int rest_min=0;
    hours = segundos / 3600;
    rest_min = segundos % 3600;
    min = rest_min / 60;
    segundos = rest_min % 60;

    printf("%d h %d min %d seg", hours, min, segundos);
}


int main()
{
    segtohour(10000);
    return 0;
}
