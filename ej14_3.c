#include <stdio.h>
#include <stdlib.h>

void funcion(void) {
    static int x = 0;
    x++;
    printf("%d\n", x);
}

//La palabra static significa que x guarda su valor anterior a la proxima llamada de funcion()


int main(void)
{   //x=0
    funcion(); // primera llamada de funcionc(), x=0 a la entrada en la funcion y x=1 a la salida
    //x=1
    funcion(); // segunda llamada : x = 1 a la entrada y 2 a la salida
    //x=2
    funcion(); // tercera llamada : x=2 a la entrada y 3 a la salida
    //x=3
    funcion();
    //x=4
    funcion();
    //x=5

    return EXIT_SUCCESS;
}

