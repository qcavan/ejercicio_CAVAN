#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double cilindro_diametro(double radio, double altura){

    return 2*radio;
}

double cilindro_circ(double radio, double altura){

    return 2*M_PI*radio;
}

double cilindro_base(double radio, double altura){
    double base = M_PI*radio*radio;

    return base;
}

double cilindro_volumen(float radio, double altura){
    double vol = M_PI*(radio*radio)*altura;

    return vol;
}


int main()
{
    double diam,circ,base, vol;
    diam=cilindro_diametro(3, 10);
    circ=cilindro_circ(3, 10);
    base=cilindro_base(3, 10);
    vol=cilindro_volumen(3, 10);
    printf("%lf %lf %lf %lf",diam, circ, base, vol);
    return 0;
}
