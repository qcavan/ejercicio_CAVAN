#include <stdio.h>
#include <stdlib.h>

float prob1(int x){

    if(x==0) return 0.2;
    else if(x==1) return 0.8;
    else return 0.0;
}

float prob2(int x){

    if(x==3) return 0.1;
    else if(x==5) return 0.55;
    else if(x==9) return 0.35;
    else return 0.0;
}

//que debemos hacer con la moneda ? No entendio el enunciado

void prob_dado(int x){

    if(x==1) printf("|     | \n|  *  | \n|     |\n\n");
    else if(x==2) printf("|    *| \n|     | \n|*    |\n\n");
    else if(x==3) printf("|*    | \n|  *  | \n|    *|\n\n");
    else if(x==4) printf("|*   *| \n|     | \n|*   *|\n\n");
    else if(x==5) printf("|*   *| \n|  *  | \n|*   *|\n\n");
    else if(x==6) printf("|*   *| \n|*   *| \n|*   *|\n\n");
    else printf("No es un numero de un dado\n");
}



int main()
{
    for(int i=0;i<8;i++) prob_dado(i);

    return 0;
}
