#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 100

int traza(const int A[][MAX_SIZE], size_t n){
    int tr=0;
    for(int i=0; i<n; i++)
        tr += A[i][i];
    return tr;
}

void suma(int A[][MAX_SIZE], size_t ncols, size_t nlin, int num){

    for(int j=0;j<nlin; j++){

        for(int i=0; i<ncols; i++){
            A[j][i]+=num;

        }
    }
}

void mult(int A[][MAX_SIZE], size_t ncols, size_t nlin, int num){

    for(int j=0;j<nlin; j++){

        for(int i=0; i<ncols; i++){
            A[j][i] = A[j][i]*num;

        }
    }
}

void transp(int A[][MAX_SIZE], int B[][MAX_SIZE], size_t ncols, size_t nlin){

    for(int j=0;j<nlin; j++){

        for(int i=0; i<ncols; i++){
            B[i][j] = A[j][i];

        }
    }
}

void tipo(const int A[][MAX_SIZE], size_t ncols, size_t nlin){
    int pos=0,neg=0,noneg=0,nopos=0;

    for(int j=0;j<nlin; j++){

        for(int i=0; i<ncols; i++){
            if(A[j][i]>=0){
                nopos=1;
                if(A[j][i]==0){
                neg=1;
            }else if(A[j][i]<=0){
                pos=1;
                noneg=1;
                if(A[j][i]==0){
                pos=1;
                }
            }

            }
        }
    }
    if(nopos==0){
        printf("matriz no positiva");

    }else if(noneg==0){
        printf("matriz no negativa");

    }else if(pos==0 ){
          printf("matriz positiva");
    }else if(neg==0){
        printf("matriz negativa");

    }

}

int det(const int A[][3], size_t n){
    int det=0;
    if(n==2){
        det = A[0][0]*A[1][1]-A[1][0]*A[0][1];

    }else if(n==3){
        det = (A[0][0]*A[1][1]*A[2][2]+A[1][0]*A[2][1]*A[0][2]+A[2][0]*A[0][1]*A[1][2])-(A[2][0]*A[1][1]*A[0][2]+A[0][0]*A[2][1]*A[1][2]+A[1][0]*A[0][1]*A[2][2]);

    }
    return det;
}

void producto(const int A[][MAX_SIZE],const int B[][MAX_SIZE], int C[][MAX_SIZE],int nlinA, int ncolA, int ncolB)
{
  int im,il,in;
  for(im=0;im<nlinA;im++)
    for(in=0;in<ncolB;in++)
      for(il=0;il<ncolA;il++)
        C[im][in]=A[im][il]*B[il][in];
 }


int maxi(const int A[][MAX_SIZE], size_t ncols, size_t nlin){
    int max=A[0][0];
    for(int j=0;j<nlin; j++){

        for(int i=0; i<ncols; i++){
            if(max < A[j][i]){
                max = A[j][i];

            }

        }
    }
    return max;
}

int max_col(const int A[][MAX_SIZE], size_t ncols, size_t nlin){
    int max=-1;
    int sum=0;
    for(int j=0;j<ncols; j++){

        for(int i=0; i<nlin; i++){
            if(A[i][j] < 0){
                sum -= A[i][j];

            }else if(A[i][j] > 0){
                sum += A[i][j];

            }


        }
        if(max < sum){
                max = sum;

        }
    }
    return max;
}

int max_fil(const int A[][MAX_SIZE], size_t ncols, size_t nlin){
    int max=-1;
    int sum=0;
    for(int j=0;j<nlin; j++){

        for(int i=0; i<ncols; i++){
            if(A[j][i] < 0){
                sum -= A[j][i];

            }else if(A[j][i] > 0){
                sum += A[j][i];

            }


        }
        if(max < sum){
                max = sum;

        }
    }
    return max;
}


int main()
{
    printf("Hello world!\n");
    return 0;
}
